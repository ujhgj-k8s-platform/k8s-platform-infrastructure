# Конфигурация GKE через Terraform 

Репозиторий содержит инструкции (IaC) для развёртывания K8s кластера в GKE.

## Начальная настройка

1. Установите [Google Cloud SDK](https://cloud.google.com/sdk/downloads) и [Terraform](https://www.terraform.io/intro/getting-started/install.html)

1. Создайте проект в GCP и запишите его в переменные окружения, это потребуется для выполнения дальнейших шагов:

    ```bash
    export GCP_PROJECT_NAME=<project_name>
    ```

1. Активируйте Kubernetes в консоли GCP.

1. Сконфигурируйте Google Cloud SDK:

    ```bash
    gcloud init
    ```

1. Создайте Service Account для пайплайна:

    ```bash
    gcloud iam service-accounts create terraform
    
    gcloud iam service-accounts keys create service_account_terraform_credentials.json \
        --iam-account terraform@$GCP_PROJECT_NAME.iam.gserviceaccount.com
    
    gcloud projects add-iam-policy-binding $GCP_PROJECT_NAME \
        --member serviceAccount:terraform@$GCP_PROJECT_NAME.iam.gserviceaccount.com \
        --role roles/editor
    ```

1. Создайте state хранилище для Terraform в Google Storage:

    ```bash
    export TF_STATE_BUCKET_NAME=${GCP_PROJECT_NAME}-terraform
    
    gsutil mb gs://$TF_STATE_BUCKET_NAME/
    
    gsutil acl ch -u terraform@$GCP_PROJECT_NAME.iam.gserviceaccount.com:OWNER \
        gs://$TF_STATE_BUCKET_NAME/
    ```

## Использование

Перед инициализацией Terraform потребуется перенести имя проекта в GCP и имя бакета для состояния (переменные $GCP_PROJECT_NAME и $TF_STATE_BUCKET_NAME, заданные выше) в конфигурационные файлы `terraform.tfvars` и `backend-gcs.tfvars`.

Инициализация Terraform:

```bash
terraform init -backend-config=backend-gcs.tfvars
```

Создание ресурсов:

```bash
terraform apply
```

Удаление ресурсов:

```bash
terraform destroy:
```

Удаление state хранилища:

```bash
gsutil rm -r gs://$GCP_PROJECT_NAME/
```

## Пайплайн

Репозиторий содержит файл .gitlab-ci.yml пайплайна для Gitlab CI.

Для того чтобы использовать пайплайн, потребуется создать защищенную, маскированную переменную `$SERVICE_ACCOUNT_TERRAFORM_CREDENTIALS_JSON_BASE64` в Gitlab CI. Значение переменной можно получить следующим образом: 

    base64 -w 0 service_account_terraform_credentials.json
