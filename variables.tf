variable project {
  description = "Project"
}

variable region {
  description = "Region"
}

variable k8s_01__zone {
  description = "Zone"
}

variable k8s_01__pool_default__node_count {
  description = "Node count in the Cluster"
  default     = 3
}

variable k8s_01__pool_default__machine_type {
  description = "Node machine type in the Cluster"
  default     = "n1-standard-1"
}

variable k8s_01__pool_default__disk_size_gb {
  description = "Node machine disk size (Gb)"
  default     = 10
}

variable k8s_01__pool_second__node_count {
  description = "Node count in the Second Pool"
}

variable k8s_01__pool_second__machine_type {
  description = "Node machine type in the Second Pool"
}

variable k8s_01__pool_second__disk_size_gb {
  description = "Node machine disk size (Gb) in the Second Pool"
}

variable k8s_01_version {
  default = "1.16.15-gke.6000"
}
