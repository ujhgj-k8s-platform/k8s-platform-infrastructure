project = "k8s-platform-coursework"
region  = "europe-west3"

k8s_01_version                     = "1.16.15-gke.4300"
k8s_01__zone                       = "europe-west3-b"

k8s_01__pool_default__node_count   = 3
k8s_01__pool_default__machine_type = "n1-standard-1"
k8s_01__pool_default__disk_size_gb = 10

k8s_01__pool_second__node_count    = 1
k8s_01__pool_second__machine_type  = "n1-standard-1"
k8s_01__pool_second__disk_size_gb  = 10
