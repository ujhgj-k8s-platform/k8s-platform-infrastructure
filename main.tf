terraform {
  backend "gcs" {
  }
}

provider "google" {
  credentials = file("./service_account_terraform_credentials.json")
  project = var.project
  region  = var.region
}

resource "google_container_cluster" "k8s_01" {
  name               = "k8s-01"
  location           = var.k8s_01__zone
  initial_node_count = var.k8s_01__pool_default__node_count

  min_master_version = var.k8s_01_version
  # node version must match master version
  # https://www.terraform.io/docs/providers/google/r/container_cluster.html#node_version
  node_version       = var.k8s_01_version

  node_config {
    machine_type = var.k8s_01__pool_default__machine_type
    disk_size_gb = var.k8s_01__pool_default__disk_size_gb
  }
}

resource "google_container_node_pool" "second" {
  name       = "second-pool"
  location   = var.k8s_01__zone
  cluster    = google_container_cluster.k8s_01.name
  node_count = var.k8s_01__pool_second__node_count

  node_config {
    machine_type = var.k8s_01__pool_second__machine_type
    disk_size_gb = var.k8s_01__pool_second__disk_size_gb
  }
}
