# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "3.50.0"
  hashes = [
    "h1:ZH2n1UfIVu5rKyqUjkhrIjExZI0ju3W/qfWUk3MOG6I=",
    "zh:194e113ccc1429d324d20a2709b979a993bbc0543ccaf3bad4b385f896323b2b",
    "zh:230ac07c6a4726ad64e16603e95f58d47bbd71dbca8709673c006fd4df14abaa",
    "zh:35483e5b8dda4c520d8f9f77592eb5b501ce8b04be47c5a93698e4d0eb67203e",
    "zh:39a1ac35e2f224c1ebb392c3c21e62a2547ef41f726c66ef53a574226b6556f7",
    "zh:a12de427187c4d36a8ed4655ee82fb466510413726424c8433a450c4b744590a",
    "zh:a1e4da62d8e4c77185c62d6e16deb4b20601773c065ad0b796e6d02777db6fe7",
    "zh:b25aaa7d72ea1b22450eb3ad202ba90d6aa36bab2d6537a302b37a205348220b",
    "zh:b7f649f96624a6d74c0fd45e06f051f6ae49130e643c61354f0ca83eeb604b41",
    "zh:df8ced9ad951b4acaf17fe8a7a6c2093bca2a8f4a347685e3baa6a08766fe3b0",
    "zh:e6556c2a4af6bed8fb3545f1f07a47fb6f2ebf91bc639cbee54981f1e5162184",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "1.11.2"
  constraints = "1.11.2"
  hashes = [
    "h1:YnBBBh0CiSoWsumw6xsproZvot8MS2LzP1GAlrawrl4=",
    "zh:00720b68dc6728ff65a2de16589c5c3551668f30664a7fc24e3673d3bc92f420",
    "zh:2e3fcc6b6ded997fc01e5ef847a860fa7e77093bebb2b19627f7a5ea877136da",
    "zh:51508ed16a0d686e74bc268ca0f8183f1f7acdd6a5214096857fd092ee2f4e68",
    "zh:5f8353e0205d199aecf14f97899307c2a97d875dc7febd4a6b05833f0d43a66a",
    "zh:6c51951744beebbb91cb2b2a8b02a00e0bd29c00e49fa7165a31b4e0bdb615d8",
    "zh:6dec3b7298bcf7e4a17590810ee7313dbf406cb5a31b2d48cab0458fd8f398ae",
    "zh:7e1a0ca002c14d36b990154c1574971d745d1ca75199bedd340d2e480650d6ec",
    "zh:8475adaed86862daaa7064fdd674bdb428773d9af0cb342d0ba1e1b0271b3adc",
    "zh:a6bee9762ef751d8cb7bd9f73474d274dbcfd9d3872b5e759df950e0dcc7c622",
    "zh:ecb9c1e900af66fe51b8ac2300816d4ef9ea104345babeb3b297bff82a3d4907",
    "zh:f0fc3d0ca43b696231050d939ef50b050d2a5b32380c44d56df9d667232bdc83",
    "zh:fae30440f1d49d57fead38a2c23746e45193735e6aa3ad305b091bc5c3c6679e",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.0.0"
  hashes = [
    "h1:EC6eh7avwx1rF56h3RZcxgEp/14ihi7Sk/4J3Hn4nIE=",
    "zh:34ce8b79493ace8333d094752b579ccc907fa9392a2c1d6933a6c95d0786d3f1",
    "zh:5c5a19c4f614a4ffb68bae0b0563f3860115cf7539b8adc21108324cfdc10092",
    "zh:67ddb1ca2cd3e1a8f948302597ceb967f19d2eeb2d125303493667388fe6330e",
    "zh:68e6b16f3a8e180fcba1a99754118deb2d82331b51f6cca39f04518339bfdfa6",
    "zh:8393a12eb11598b2799d51c9b0a922a3d9fadda5a626b94a1b4914086d53120e",
    "zh:90daea4b2010a86f2aca1e3a9590e0b3ddcab229c2bd3685fae76a832e9e836f",
    "zh:99308edc734a0ac9149b44f8e316ca879b2670a1cae387a8ae754c180b57cdb4",
    "zh:c76594db07a9d1a73372a073888b672df64adb455d483c2426cc220eda7e092e",
    "zh:dc09c1fb36c6a706bdac96cce338952888c8423978426a09f5df93031aa88b84",
    "zh:deda88134e9780319e8de91b3745520be48ead6ec38cb662694d09185c3dac70",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}
